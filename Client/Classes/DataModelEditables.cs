﻿using System.Collections.Generic;
using System.IO;
using Services.Classes;
using System.IO.Ports;
using System.Runtime.Remoting.Channels;
using FileAttributes = Services.Classes.FileAttributes;


namespace FilemapBackupSystem.Classes
{
    class DataModelEditables
    {
        //public string RootDir = @"c:\Users\techcode\Dropbox\Work\Система резервного копирования облака\Files\Server\";
        //public string FilesDirName = @"";
        //public int ReadWriteBuffer = 10485760; // 10mb

        public int ReadWriteBuffer;
        public long ChunkSize;
        //public long XorUnitSize;
        
        public FileAttributes LocalFile;
        public FileAttributes MergedFile;
        public FileAttributes CopyFilemap;
        public FileAttributes XorFilemap;

        public Exceptioner Exceptioner;
        public FileWorker FileWorker;
        public FilemapWorker FilemapWorker;
        public DirectoryWorker DirectoryWorker;
        public Viewer Viewer;
        public Logger Logger;

        public ClientNetworker ClientNetworker;

        public DataModelEditables(string targetFileFullName, long chunkSize, int readWriteBuffer)
        {
            ChunkSize = chunkSize;
            ReadWriteBuffer = readWriteBuffer;

            Exceptioner = new Exceptioner();
            FileWorker = new FileWorker();
            FilemapWorker = new FilemapWorker();
            ClientNetworker = new ClientNetworker();         

            DirectoryWorker = new DirectoryWorker();
            DirectoryWorker.Init(targetFileFullName);

            Logger = new Logger(DirectoryWorker.LogsFullName);
            Viewer = new Viewer(Logger);

            LocalFile = new FileAttributes { Chunks = new List<Chunk>(), Filename = DirectoryWorker.LocalFileFullName };
            MergedFile = new FileAttributes { Chunks = new List<Chunk>(), Filename = DirectoryWorker.MergedFileFullName };
            //ReceivedFile = new FileAttributes {Chunks = new List<Chunk>(), Filename = ReceivedFileName};

            CopyFilemap = new FileAttributes
            {
                Chunks = new List<Chunk>(),
                FileMapName = DirectoryWorker.CopyFileMapFullName
            };
            XorFilemap = new FileAttributes
            {
                Chunks = new List<Chunk>(),
                FileMapName = DirectoryWorker.XorFilemapFullName
            };
        }


    }
}

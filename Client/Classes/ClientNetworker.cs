﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Classes;

namespace FilemapBackupSystem.Classes
{
    class ClientNetworker : Services.Classes.NetWorker
    {
        public void Connect(string ipAddress, int port)
        {
            base.CInitClient(ipAddress, port);
        }

        public override void SendFile(string srcFileName, string sendFileName, Int64 offset, Int64 length)
        {
            base.SendFile(srcFileName, sendFileName, offset, length);
        }

        public override string ReceiveFile(string dstDir)
        {
            return base.ReceiveFile(dstDir);
        }


    }
}

﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using Services.Classes;
using System.Diagnostics;

namespace FilemapBackupSystem.Classes
{
    class Controller
    {
        private readonly DataModelEditables _dme;

        public Controller(string targetFileFullName, long chunkSize, int readWriteBuffer)
        {
            _dme = new DataModelEditables(targetFileFullName, chunkSize, readWriteBuffer);
        }

        // 1. на сервер отправляем карту копирования. 
        //      *если она пустая - делим весь файл на чанки, формируем из этого 
        // copyFilemap(без хэшей), передаем
        // карту и все чанки клиенту
        //      *если не пустая - считываем ее, делим локальный файл на части
        // среди карты ксор ищем схожие области. если схожая область из xorFilemap:
        //          * статичная - читаем эту область из файла, считаем ее хэш, сравниваем с
        //          хэшем из xorFilemap. если не совпали - добавляем в copyFilemap
        //          * иные виды совпавших областей добавляем в copyFilemap
        // если схожей области не нашлось - добавляем ее в copyFilemap как Extra
        // 2. сохраняем xorFilemap
        // 3. извлечь области, вошедшие в copyFilemap, в ChunksTempDir
        public void ClientWork(string ipAddress, int port)
        {
            _dme.Logger.LogString(Environment.NewLine + Environment.NewLine + Environment.NewLine, true);
            _dme.Logger.LogString("ОЧЕРЕДНОЙ ЗАПУСК", true);
            _dme.Logger.LogString(Environment.NewLine + Environment.NewLine + Environment.NewLine, true);
            TimeKeeper tk1 = new TimeKeeper(true);
            tk1.AttachLogger(_dme.Logger);
            tk1.Start("Весь процесс копирования");

            _dme.Viewer.DescribeInitSettings(_dme.DirectoryWorker.LocalFileFullName, _dme.ChunkSize,
                                             _dme.ReadWriteBuffer);

            _dme.ClientNetworker.Connect(ipAddress, port);

            _dme.ClientNetworker.SendMessage(NetCommands.InitBackup);

            if (_dme.ClientNetworker.ReceiveMessage() == NetCommands.BackupInited)
            {
                _dme.Viewer.PrintInfo("Запущен процесс резервного копирования", true);
                InitBackup();
            }

            tk1.Stop();
        }

        public void InitBackup()
        {
            TimeKeeper tk2 = new TimeKeeper(true);
            tk2.AttachLogger(_dme.Logger);
            tk2.Start("Подготовка (до передачи чанков)");

            _dme.FileWorker.SplitFileToChunks(ref _dme.LocalFile, _dme.ChunkSize, false, _dme.ReadWriteBuffer);
            _dme.Viewer.DescribeFile(_dme.LocalFile);

            // принять xorFilemap
            _dme.ClientNetworker.SendMessage(NetCommands.SendXorFilemap);
            _dme.ClientNetworker.ReceiveFile(_dme.DirectoryWorker.FilesDirFullName); // <

            LoadFilemap(FileMapTypes.XorFileMap);
            _dme.Viewer.PrintInfo("Ксор-карта получена и считана успешно", true);
            _dme.Viewer.DescribeFile(_dme.XorFilemap);

            // составить карту копирования
            _dme.FileWorker.GenerateCopyFilemap(_dme.LocalFile, _dme.XorFilemap, _dme.CopyFilemap, _dme.ReadWriteBuffer);
            SaveFilemap(FileMapTypes.CopyFileMap);
            _dme.Viewer.PrintInfo(string.Format("Копи-карта создана успешно, областей всего - {0}", _dme.CopyFilemap.Chunks.Count), true);
            _dme.Viewer.DescribeFile(_dme.CopyFilemap);

            // отослать карту копирования клиенту
            _dme.ClientNetworker.SendFile(_dme.DirectoryWorker.CopyFileMapFullName, _dme.DirectoryWorker.CopyFileMapName, 0, 0); // >
            _dme.Viewer.PrintInfo("Копи-карта отправлена", true);

            tk2.Stop();

            TimeKeeper tk3 = new TimeKeeper(true);
            tk3.AttachLogger(_dme.Logger);
            tk3.Start("Передача чанков по сети");
            // отправить измененные чанки клиенту
            for (int i = 0; i < _dme.CopyFilemap.Chunks.Count; i++)
            {
                if (_dme.ClientNetworker.ReceiveMessage() == NetCommands.SendChunk)
                {
                    _dme.ClientNetworker.SendFile(_dme.LocalFile.Filename,
                                                  _dme.CopyFilemap.Chunks[i].Id.ToString(CultureInfo.InvariantCulture),
                                                  _dme.CopyFilemap.Chunks[i].Start, _dme.CopyFilemap.Chunks[i].Length);
                    _dme.Viewer.PrintInfo(string.Format("Выполнено {0}/{1}", i+1, _dme.CopyFilemap.Chunks.Count), true);
                }
            }
            tk3.Stop();
            _dme.ClientNetworker.CCloseConnection();

            Cleaner.Clear(_dme.DirectoryWorker.ChunksTempDirFullName, _dme.CopyFilemap);
        }

        public void SaveFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FileMapTypes.CopyFileMap:
                    _dme.FilemapWorker.SaveFilemap(_dme.CopyFilemap);
                    break;
                case FileMapTypes.XorFileMap:
                    _dme.FilemapWorker.SaveFilemap(_dme.XorFilemap);
                    break;
            }
        }

        public void LoadFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FileMapTypes.CopyFileMap:
                    _dme.FilemapWorker.LoadFilemap(ref _dme.CopyFilemap);
                    break;
                case FileMapTypes.XorFileMap:
                    _dme.FilemapWorker.LoadFilemap(ref _dme.XorFilemap);
                    break;
            }
        }
    }
}

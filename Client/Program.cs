﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FilemapBackupSystem.Classes;
using Services.Classes;

namespace FilemapBackupSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Exceptioner exr = new Exceptioner();
            try
            {
                Controller controller = new Controller(args[0], long.Parse(args[1]), int.Parse(args[2]));
                controller.ClientWork(args[3], int.Parse(args[4]));
            }
            catch (Exception ex)
            {
                exr.CatchExceprion(ex);
            }
            finally
            {
                Console.WriteLine("Нажмите любую кнопку...");
                Console.ReadKey();
            }
        }
    }
}

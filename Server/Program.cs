﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FilemapBackupSystem.Classes;
using Services.Classes;

namespace FilemapBackupSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Exceptioner exr = new Exceptioner();
            try
            {
                Controller controller = new Controller(args[0], long.Parse(args[1]), long.Parse(args[2]), int.Parse(args[3]));
                controller.ServerWork(args[4], int.Parse(args[5]));
            }
            catch (Exception ex)
            {
                exr.CatchExceprion(ex);
            }
            finally
            {
                Console.WriteLine("Нажмите любую кнопку...");
                Console.ReadKey();
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.IO;
using Services.Classes;
using FileAttributes = Services.Classes.FileAttributes;

namespace FilemapBackupSystem.Classes
{
    class DataModelEditables
    {
        public int ReadWriteBuffer;
        public long ChunkSize;
        public long XorUnitSize;

        public FileAttributes LocalFile;
        public FileAttributes MergedFile;

        public FileAttributes CopyFilemap;
        public FileAttributes XorFilemap;

        public Viewer Viewer;

        public Exceptioner Exceptioner;
        public FileWorker FileWorker;
        public DirectoryWorker DirectoryWorker;
        public Logger Logger;

        public FilemapWorker FilemapWorker;

        public ServerNetworker ServerNetworker;

        public DataModelEditables(string targetFileFullName, long chunkSize, long xorUnitSize, int readWriteBuffer)
        {
            ChunkSize = chunkSize;
            ReadWriteBuffer = readWriteBuffer;
            XorUnitSize = xorUnitSize;

            Exceptioner = new Exceptioner();
            FileWorker = new FileWorker();

            FilemapWorker = new FilemapWorker();
            ServerNetworker = new ServerNetworker();

            DirectoryWorker = new DirectoryWorker();
            DirectoryWorker.Init(targetFileFullName);

            Logger = new Logger(DirectoryWorker.LogsFullName);
            Viewer = new Viewer(Logger);

            LocalFile = new FileAttributes {Chunks = new List<Chunk>(), Filename = DirectoryWorker.LocalFileFullName};
            MergedFile = new FileAttributes {Chunks = new List<Chunk>(), Filename = DirectoryWorker.MergedFileFullName};
            

            CopyFilemap = new FileAttributes
            {
                Chunks = new List<Chunk>(),
                FileMapName = DirectoryWorker.CopyFileMapFullName
            };
            XorFilemap = new FileAttributes
            {
                Chunks = new List<Chunk>(),
                FileMapName = DirectoryWorker.XorFilemapFullName
            };
        }


    }
}

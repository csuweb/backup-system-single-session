﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Services.Classes;

namespace FilemapBackupSystem.Classes
{
    class ServerNetworker : Services.Classes.NetWorker
    {
        public void WaitClient(string ipAddress = "localhost", int port = 1488)
        {
            base.SInitServer(ipAddress, port);
            base.SWaitClient();
        }

        public override void SendFile(string srcFileName, string sendFileName, long start, long length)
        {
            base.SendFile(srcFileName, sendFileName, start, length);
        }

        public override string ReceiveFile(string dstDir)
        {
            return base.ReceiveFile(dstDir);
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.IO;
using Services.Classes;

namespace FilemapBackupSystem.Classes
{
    class Controller
    {
        private readonly DataModelEditables _dme;

        public Controller(string targetFileFullName, long chunkSize, long xorUnitSize, int readWriteBuffer)
        {
            this._dme = new DataModelEditables(targetFileFullName, chunkSize, xorUnitSize, readWriteBuffer);
        }

        // с сервера переданы чанки файла в ChunksTempDir, локальный файл
        // отсутствует
        // 0. считать полученный от сервера copyFilemap
        // 1. слить эти чанки в правильной последовательности
        // 2. поделить полученный файл на чанки, посчитать хэши чанков
        // 3. сохранить карту как xorFilemap
        public void ServerWork(string ipAddress, int port)
        {

            _dme.Logger.LogString(Environment.NewLine + Environment.NewLine + Environment.NewLine, true);
            _dme.Logger.LogString("ОЧЕРЕДНОЙ ЗАПУСК", true);
            _dme.Logger.LogString(Environment.NewLine + Environment.NewLine + Environment.NewLine, true);

            TimeKeeper tk1 = new TimeKeeper(true);
            tk1.AttachLogger(_dme.Logger);
            tk1.Start("Весь процесс резервного копирования");

            _dme.Viewer.DescribeInitSettings(_dme.DirectoryWorker.LocalFileFullName, _dme.ChunkSize,
                                             _dme.ReadWriteBuffer);

            _dme.ServerNetworker.WaitClient(ipAddress, port);

            string clientCommand = _dme.ServerNetworker.ReceiveMessage();
            _dme.Viewer.PrintInfo(clientCommand, true);

            if (clientCommand == NetCommands.InitBackup)
            {
                _dme.Viewer.PrintInfo("Запущен процесс резервного копирования", true);
                _dme.ServerNetworker.SendMessage(NetCommands.BackupInited);
                InitBackup();
            }

            tk1.Stop();
        }

        public void InitBackup()
        {
            TimeKeeper tk2 = new TimeKeeper(true);
            tk2.AttachLogger(_dme.Logger);
            tk2.Start("Подготовительный этап (до принятия чанков по сети)");

            LoadFilemap(FileMapTypes.XorFileMap);

            if (!_dme.FileWorker.IsFileExists(_dme.LocalFile) &&
                !_dme.XorFilemap.IsSplitted)
            {
                // если локальный файл и xorFilemap не были обнаружены - 
                // создать и сохранить пустой xorFilemap
                _dme.Viewer.PrintInfo("Ксор-карта не была обнаружена - будет создана пустая", true);
                SaveFilemap(FileMapTypes.XorFileMap);
                _dme.Viewer.DescribeFile(_dme.XorFilemap);

            }
            else if (_dme.FileWorker.IsFileExists(_dme.LocalFile) &&
                     _dme.XorFilemap.IsSplitted)
            {
                _dme.Viewer.PrintInfo("Обнаружена локальная ксор-карта", true);
                _dme.FileWorker.SplitFileToChunks(ref _dme.LocalFile, _dme.ChunkSize,
                    false, _dme.ReadWriteBuffer);
                _dme.Viewer.DescribeFile(_dme.LocalFile);
            }
            else
            {
                throw new Exception("Недостающие файлы!");
            }

            // отправить xorFilemap на сервер
            if (_dme.ServerNetworker.ReceiveMessage() == NetCommands.SendXorFilemap)
                _dme.ServerNetworker.SendFile(_dme.XorFilemap.FileMapName,
                                              _dme.DirectoryWorker.XorFileMapName, 0, 0);
            
            // получить copyFilemap от сервера
            _dme.ServerNetworker.ReceiveFile(_dme.DirectoryWorker.RootDirFullName);
            LoadFilemap(FileMapTypes.CopyFileMap);
            _dme.Viewer.PrintInfo(string.Format("{0} чанков будут приняты", _dme.CopyFilemap.Chunks.Count), true);
            _dme.Viewer.DescribeFile(_dme.CopyFilemap);

            tk2.Stop();

            TimeKeeper tk3 = new TimeKeeper(true);
            tk3.AttachLogger(_dme.Logger);
            tk3.Start("Принятие чанков по сети");

            // получить чанки во временную папку
            for (int i = 0; i < _dme.CopyFilemap.Chunks.Count; i++)
            {
                _dme.ServerNetworker.SendMessage(NetCommands.SendChunk);
                _dme.ServerNetworker.ReceiveFile(_dme.DirectoryWorker.ChunksTempDirFullName);
                _dme.Viewer.PrintInfo(string.Format("Выполнено {0}/{1}", i+1, _dme.CopyFilemap.Chunks.Count), true);
            }

            tk3.Stop();

            TimeKeeper tk4 = new TimeKeeper(true);
            tk4.AttachLogger(_dme.Logger);
            tk4.Start("Формирование карты xor");

            // поксорить локальный файл с загруженными чанками
            if (_dme.FileWorker.IsFileExists(_dme.LocalFile) &&
                _dme.XorFilemap.IsSplitted && _dme.CopyFilemap.IsSplitted)
            {
                _dme.Viewer.PrintInfo("Формируется новая ксор-карта на основе полученных данных", true);
                _dme.FileWorker.XorFileToChunks(_dme.LocalFile,
                                                              _dme.DirectoryWorker.ChunksTempDirFullName,
                                                              ref _dme.XorFilemap,
                                                              _dme.CopyFilemap,
                                                              _dme.XorUnitSize,
                                                              _dme.ReadWriteBuffer);
            }

            if (_dme.CopyFilemap.IsSplitted)
            {
                _dme.Viewer.PrintInfo("Полученные чанки сливаются в единный файл", true);
                _dme.FileWorker.MergeChunks(_dme.LocalFile, _dme.CopyFilemap,
                                                          ref _dme.MergedFile,
                                                          _dme.DirectoryWorker.ChunksTempDirFullName,
                                                          _dme.ReadWriteBuffer);
            }

            if (!_dme.XorFilemap.IsSplitted)
            {
                _dme.FileWorker.SplitFileToChunks(ref _dme.LocalFile, _dme.ChunkSize, false, _dme.ReadWriteBuffer);
                _dme.Viewer.PrintInfo("Формируется ксор-карта по-умолчанию", false);
                _dme.FileWorker.CreateDefaultXorFilemap(_dme.LocalFile,
                    ref _dme.XorFilemap, _dme.ChunkSize, _dme.ReadWriteBuffer);
            }
            SaveFilemap(FileMapTypes.XorFileMap);
            _dme.Viewer.PrintInfo("Ксор-карта сохранена", true);

            tk4.Stop();
            _dme.ServerNetworker.SCloseConnection();
            Cleaner.Clear(_dme.DirectoryWorker.ChunksTempDirFullName, _dme.CopyFilemap);
        }

        public void SaveFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FileMapTypes.CopyFileMap:
                    _dme.FilemapWorker.SaveFilemap(_dme.CopyFilemap);
                    break;
                case FileMapTypes.XorFileMap:
                    _dme.FilemapWorker.SaveFilemap(_dme.XorFilemap);
                    break;
            }
        }

        public void LoadFilemap(string filemapType)
        {
            switch (filemapType)
            {
                case FileMapTypes.CopyFileMap:
                    _dme.FilemapWorker.LoadFilemap(ref _dme.CopyFilemap);
                    break;
                case FileMapTypes.XorFileMap:
                    _dme.FilemapWorker.LoadFilemap(ref _dme.XorFilemap);
                    break;
            }
        }
    }
}

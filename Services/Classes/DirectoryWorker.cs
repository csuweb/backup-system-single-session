﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Classes
{
    public class DirectoryWorker
    {
        public string RootDirFullName;
        public string LocalFileName;

        public string ChunksTempDirName;
        public string FilesDirName;

        public string LogsName
        {
            get { return "logs.log"; }
        }

        public string FileMapFormat
        {
            get { return ".xml"; }
        }

        public string MergedFileName
        {
            get { return ("Merged.tmp"); }
        }

        public string XorFileMapName
        {
            get { return "xorFilemap" + FileMapFormat; }
        }

        public string CopyFileMapName
        {
            get { return "copyFilemap" + FileMapFormat; }
        }

        public string LocalFileFullName
        {
            get { return Path.Combine(RootDirFullName, LocalFileName); }
        }

        public void Init(string targetFileFullFilename, string filesDirName = "", string chunksTempDirName = "chunksTempDir")
        {
            ChunksTempDirName = chunksTempDirName;
            RootDirFullName = Path.GetDirectoryName(targetFileFullFilename);
            LocalFileName = Path.GetFileName(targetFileFullFilename);
            FilesDirName = filesDirName;
        }

        public string LogsFullName
        {
            get { return Path.Combine(RootDirFullName, LogsName); }
        }

        public string FilesDirFullName
        {
            get { return Path.Combine(RootDirFullName, FilesDirName); }
        }

        public string MergedFileFullName
        {
            get { return Path.Combine(FilesDirFullName, MergedFileName); }
        }

        public string XorFilemapFullName
        {
            get { return Path.Combine(FilesDirFullName, XorFileMapName); }
        }

        public string CopyFileMapFullName
        {
            get { return Path.Combine(FilesDirFullName, CopyFileMapName); }
        }

        public string ChunksTempDirFullName
        {
            get { return Path.Combine(FilesDirFullName, ChunksTempDirName); }
        }
    }
}

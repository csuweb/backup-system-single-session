﻿using System;
using System.Collections.Generic;

namespace Services.Classes
{
    public struct Chunk
    {
        public Int32 Id;
        public string Hash;
        public string Type;

        public Int64 Start;
        public Int64 End;
        public Int64 Length;
        public void CopyFrom(Chunk in1)
        {
            Id = in1.Id;
            Hash = in1.Hash;
            Type = in1.Type;

            Start = in1.Start;
            End = in1.End;
            Length = in1.Length;
        }
    }

    public struct XorUnitAttributes
    {
        public bool IsEqual;
        public Int64 DifferenceOffset;
    }

    public struct FileAttributes
    {
        public bool IsSplitted;
        public string FileMapName;
        public string Filename;
        public Int64 FileSize;
        public List<Chunk> Chunks;
        public string Hash;
        public void CopyFrom(FileAttributes in1)
        {
            if(in1.FileMapName != null) FileMapName = in1.FileMapName;
            Hash = in1.Hash;
            Filename = in1.Filename;
            FileSize = in1.FileSize;
            Chunks = new List<Chunk>(in1.Chunks.Count);
            foreach (var chunk in in1.Chunks)
                Chunks.Add(chunk);
        }

        public Chunk GetChunkById(int id)
        {
            Chunk result = new Chunk();
            for (int i = 0; i < Chunks.Count; i++)
            {
                if (Chunks[i].Id == id)
                    result.CopyFrom(Chunks[i]);
            }
            return result;
        }

        public void Destroy()
        {
            IsSplitted = false;
            Chunks.Clear();
            Filename = null;
            FileSize = 0;
            Hash = null;
        }
    }

    public static class ChunkType
    {
        public const string Static = "Static";
        public const string Variable = "Variable";
        public const string Extra = "Extra";
        public const string DifLen = "DifLen";
        public const string Undef = "Undef";
    }

    public static class NetCommands
    {
        public const string InitBackup = "NEED_BACKUP";
        public const string BackupInited = "BACKUP_INITED";

        public const string ChunksSendStarted = "CHUNKS_SEND_STARTED";
        public const string WaitForChunks = "WAIT_FOR_CHUNKS";
       


        public const string SendXorFilemap = "SEND_XOR_FILEMAP";

        public const string SendCopyFileMap = "SEND_COPY_FILEMAP";
        public const string SendCopyFilemapWait = "WAIT_COPY_FILEMAP";
        public const string CopyFileMapSent = "SEND_COPY_FILEMAP_OK";

        public const string SendChunk = "SEND_CHUNK";

        public const string ReceiveCopyFilemap = "RECEIVE_COPY_FILEMAP";
    }

    public static class FileMapTypes
    {
        public const string LocalFileMap = "localFilemap";
        public const string ReceivedFileMap = "receivedFilemap";
        public const string CopyFileMap = "copyFilemap";
        public const string XorFileMap = "xorFilemap";
    }

    public static class Const
    {
        public const Int64 ChunkSize = 5242880; //5 mb
        public const Int64 XorUnitSize = 2097152; //2mb, меньше chunkSize
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Services.Classes
{
    public class TimeKeeper
    {
        private Stopwatch _totalTimeElapsed;

        private Logger _lg;
        private bool _lgAttached;

        private string _operationName;
        private readonly bool _toPrintInConsole;

        public TimeKeeper(bool toPrintOnConsole)
        {
            _toPrintInConsole = toPrintOnConsole;
        }

        public void AttachLogger(Logger lg)
        {
            _lg = lg;
            _lgAttached = true;
        }

        public void Start(string operationName)
        {
            _totalTimeElapsed = new Stopwatch();
            _totalTimeElapsed.Start();
            _operationName = operationName;
            string message = string.Format("Время начала операции \"{0}\" - {1}", _operationName, DateTime.Now);

            if (_lgAttached) _lg.LogString(message, true);
            if (_toPrintInConsole) Console.WriteLine(message);
        }

        public string Stop()
        {
            _totalTimeElapsed.Stop();
            string message = string.Format("Время окончания операции \"{0}\" - {1}", _operationName, DateTime.Now);

            if (_lgAttached) _lg.LogString(message, true);
            if (_toPrintInConsole) Console.WriteLine(message);

            TimeSpan ts = _totalTimeElapsed.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);

            message = string.Format("Операция \"{0}\" длилась {1}", _operationName, elapsedTime);

            if (_lgAttached)
                _lg.LogString(message, true);

            if(_toPrintInConsole)
                Console.WriteLine(message);

            return elapsedTime;
        }
    }
}

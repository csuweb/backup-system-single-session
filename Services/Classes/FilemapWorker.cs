﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Services.Classes
{
    public class FilemapWorker
    {
        // ЗАГРУЗИТЬ ЛОКАЛЬНУЮ КАРТУ
        //  * загружается файл с именем из filemap.FileMapName
        public void LoadFilemap(ref FileAttributes filemap)
        {
            if (!File.Exists(filemap.FileMapName))
            {
                filemap.Destroy();
                return;
            }

            XDocument xmlDocument = XDocument.Load(filemap.FileMapName);

            if (xmlDocument.Element("filemapworker").Element("fileinfo").Element("targetfilename").Attribute("value") !=
                null)
                filemap.Filename =
                    xmlDocument.Element("filemapworker")
                               .Element("fileinfo")
                               .Element("targetfilename")
                               .Attribute("value")
                               .Value;

            if (xmlDocument.Element("filemapworker").Element("fileinfo").Element("filesize").Attribute("value") !=
                null)
                Int64.TryParse(
                    xmlDocument.Element("filemapworker")
                               .Element("fileinfo")
                               .Element("filesize")
                               .Attribute("value")
                               .Value, out filemap.FileSize);

            foreach (XElement filemapChunk in xmlDocument.Root.Element("chunks").Elements("chunk"))
            {
                Chunk tempChunk = new Chunk();
                Int32.TryParse(filemapChunk.Attribute("id").Value, out tempChunk.Id);

                if (filemapChunk.Attribute("hash") != null) tempChunk.Hash = filemapChunk.Attribute("hash").Value;
                if (filemapChunk.Attribute("type") != null) tempChunk.Type = filemapChunk.Attribute("type").Value;

                Int64.TryParse(filemapChunk.Attribute("start").Value, out tempChunk.Start);
                Int64.TryParse(filemapChunk.Attribute("end").Value, out tempChunk.End);
                Int64.TryParse(filemapChunk.Attribute("length").Value, out tempChunk.Length);

                filemap.Chunks.Add(tempChunk);
            }

            filemap.IsSplitted = filemap.Chunks.Count > 0;
        }

        // СОХРАНИТЬ КАРТУ
        //  * сохраняется под именем filemap.FileMapName
        public void SaveFilemap(FileAttributes filemap)
        {
            XDocument xmlDocument = new XDocument(
                new XDeclaration("1.0", "utf-8", "true"),
                new XElement("filemapworker")
                );

            xmlDocument.Element("filemapworker").Add(new XElement("fileinfo"));
            xmlDocument.Element("filemapworker").Element("fileinfo").Add(new XElement("targetfilename"));
            xmlDocument.Element("filemapworker").Element("fileinfo").Add(new XElement("filesize"));

            xmlDocument.Element("filemapworker").Element("fileinfo").Element("targetfilename").SetAttributeValue("value", filemap.Filename);
            xmlDocument.Element("filemapworker").Element("fileinfo").Element("filesize").SetAttributeValue("value", filemap.FileSize);

            XElement chunksElement = new XElement("chunks");
            xmlDocument.Element("filemapworker").Add(chunksElement);

            foreach (Chunk chunk in filemap.Chunks)
            {
                XElement filemapChunk = new XElement("chunk");

                filemapChunk.SetAttributeValue("id", chunk.Id.ToString());
                filemapChunk.SetAttributeValue("type", chunk.Type);
                filemapChunk.SetAttributeValue("start", chunk.Start.ToString());
                filemapChunk.SetAttributeValue("end",  chunk.End.ToString());
                filemapChunk.SetAttributeValue("length", chunk.Length.ToString());
                filemapChunk.SetAttributeValue("hash", chunk.Hash);

                xmlDocument.Element("filemapworker").Element("chunks").Add(filemapChunk);
            }

            if(File.Exists(filemap.FileMapName))
                File.Delete(filemap.FileMapName);

            xmlDocument.Save(filemap.FileMapName);
        }

        // ИЗВЛЕЧЬ ЧАНКИ ОПРЕДЕЛЕННОГО ВИДА ИЗ ОБЩЕГО СПИСКА
        public List<Chunk> ExtractChunksByType(string[] chunkTypes, List<Chunk> chunks)
        {
            List<Chunk> extractedChunks = new List<Chunk>();
            for (int i = 0; i < chunks.Count; i++)
            {
                if(chunkTypes.Contains(chunks[i].Type))
                    extractedChunks.Add(chunks[i]); 
            }
            return extractedChunks;
        }


    }
}

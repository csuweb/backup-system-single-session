﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Services.Classes
{
    public class NetWorker
    {
        public TcpListener SListener { get; set; }
        public TcpClient CClient { get; set; }
        public NetworkStream Stream { get; set; }

        public virtual void CInitClient(string ipAddress, int port)
        {
            CClient = new TcpClient(ipAddress, port);
            Stream = CClient.GetStream();
            Console.WriteLine("Соединен с " + ipAddress + " : " + port);
        }

        public virtual void SInitServer(string ipAddress, int port)
        {
            SListener = new TcpListener(IPAddress.Parse(ipAddress), port);
            SListener.Server.SendTimeout = 0;
            SListener.Server.ReceiveTimeout = 0;
        }

        public virtual void CloseStream()
        {
            if (Stream != System.IO.Stream.Null) Stream.Close();
        }

        public virtual void CCloseConnection()
        {
            CloseStream();
            CClient.Close();
        }

        public virtual void SCloseConnection()
        {
            CloseStream();
            SListener.Stop();
        }

        public virtual void SWaitClient()
        {
            SListener.Start();
            Console.WriteLine("Сервер ожидает подключение на " +
                              IPAddress.Parse(((IPEndPoint) SListener.LocalEndpoint).Address.ToString()) + " : " +
                              ((IPEndPoint) SListener.LocalEndpoint).Port.ToString(CultureInfo.InvariantCulture));
            CClient = SListener.AcceptTcpClient();
            Stream = CClient.GetStream();
        }

        public virtual string ReceiveFile(string dstDir)
        {
            byte[] buffer = new byte[CClient.ReceiveBufferSize];
            string fileName = "Ошибка";

            try
            {
                // считать инфо - заголовок
                int bytesRead = Stream.Read(buffer, 0, 12);
                if (bytesRead == 0) return fileName;

                ushort id = BitConverter.ToUInt16(buffer, 0);
                long len = BitConverter.ToInt64(buffer, 2);
                ushort nameLen = BitConverter.ToUInt16(buffer, 10);

                Stream.Read(buffer, 0, nameLen);

                fileName = Encoding.UTF8.GetString(buffer, 0, nameLen);

                if (id == 1 && dstDir != null)
                {
                    using (
                        BinaryWriter writer =
                            new BinaryWriter(new FileStream(Path.Combine(dstDir, fileName), FileMode.Create)))
                    {
                        int recieved = 0;
                        while (recieved < len)
                        {
                            bytesRead = Stream.Read(buffer, 0, CClient.ReceiveBufferSize);
                            recieved += bytesRead;
                            writer.Write(buffer, 0, bytesRead);
                        }
                    }
                    Console.WriteLine("Входящий файл принят - \"{0}\", [{1} байт]\r", fileName, len);
                }
                else if (!(id == 0 && dstDir == null))
                {
                    throw new Exception("Получен неожиданный файл/сообщение :)");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Stream.Close();
                CClient.Close();
            }
            finally
            {
                Stream.Flush();
            }
            return fileName;
        }

        public virtual void SendFile(string srcFileName, string sendFileName, Int64 offset, Int64 length)
        {
            FileInfo file = new FileInfo(srcFileName);

            long bytesToSend = length == 0 ? file.Length : length;

            // служебная информация
            byte[] bId = BitConverter.GetBytes((ushort) 1);
            byte[] bSize = BitConverter.GetBytes(bytesToSend);
            byte[] bFileName = Encoding.UTF8.GetBytes(sendFileName);
            byte[] bFileNameLenght = BitConverter.GetBytes((ushort) bFileName.Length);
            byte[] bFileInfo = new byte[12 + bFileName.Length];

            bId.CopyTo(bFileInfo, 0);
            bSize.CopyTo(bFileInfo, 2);
            bFileNameLenght.CopyTo(bFileInfo, 10);
            bFileName.CopyTo(bFileInfo, 12);

            Stream.Write(bFileInfo, 0, bFileInfo.Length);

            byte[] buffer = new byte[bytesToSend];

            using (FileStream fileStream = new FileStream(file.FullName, FileMode.Open))
            {
                Int64 totalRead = 0;
                fileStream.Seek(offset, SeekOrigin.Begin);
                while (totalRead < bytesToSend)
                {
                    int bytesToRead = totalRead + buffer.Length > bytesToSend
                        ? (int) (buffer.Length - totalRead)
                        : buffer.Length;

                    int bytesRead = fileStream.Read(buffer, 0, bytesToRead);

                    if(bytesToRead != bytesRead)
                        throw new Exception(string.Format("Не удалось считать файл {0}", sendFileName));

                    Stream.Write(buffer, 0, bytesRead);
                    totalRead += bytesRead;
                }
                if(totalRead != bytesToSend)
                    throw new Exception(String.Format("Файл {0} мог быть отправлен не полностью", sendFileName));
            }
                
            Console.WriteLine("Файл \"{0}\" отправлен [{1} байт]\r", sendFileName, length);
            Stream.Flush();
        }

        public virtual void SendMessage(string message)
        {
            byte[] bId = BitConverter.GetBytes((ushort)0);
            byte[] bMessage = Encoding.UTF8.GetBytes(message);
            byte[] bMessageLenght = BitConverter.GetBytes((ushort)bMessage.Length);
            byte[] bFileInfo = new byte[12 + bMessage.Length];

            bId.CopyTo(bFileInfo, 0);
            bMessageLenght.CopyTo(bFileInfo, 10);
            bMessage.CopyTo(bFileInfo, 12);

            Stream.Write(bFileInfo, 0, bFileInfo.Length);
            Stream.Flush();
        }

        public virtual string ReceiveMessage()
        {
            return ReceiveFile(null);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Services.Classes
{
    public class Logger
    {
        public string LogFileName;

        public Logger(string logFileName)
        {
            LogFileName = logFileName;
            if (!File.Exists(LogFileName))
            {
                FileStream fs = File.Create(LogFileName);
                fs.Close();
            }
        }

        public void LogString(string message, bool EndNewLine)
        {
            if (EndNewLine)
                File.AppendAllText(LogFileName, message + Environment.NewLine);
            else
                File.AppendAllText(LogFileName, message);
        }
    }
}

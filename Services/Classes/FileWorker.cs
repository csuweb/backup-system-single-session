﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Services.Classes
{
    public class FileWorker
    {
        private object lock1 = new object();
        private object lock2 = new object();

        // ПРОВЕРИТЬ СУЩЕСТВОВАНИЕ ФАЙЛА
        public void CheckFileExists(FileAttributes file)
        {
            if (!File.Exists(file.Filename))
                throw new Exception(string.Format("Файл \"{0}\" недоступен", file.Filename));
        }

        // ПРОВЕРИТЬ СУЩЕСТВОВАНИЕ ДИРРЕКТОРИИ
        public void CheckDirExists(string dir)
        {
            if (!Directory.Exists(dir))
                throw new Exception(string.Format("Директория \"{0}\" недоступна", dir));
        }

        // ВЫЧИСЛИТЬ MD5-ХЭШ ФАЙЛА
        public string GenerateFileHash(string fileName)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fileName))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }

        // ПОСЧИТАТЬ КОЛИЧЕСТВО ЧАНКОВ В ФАЙЛ
        //  * округление в большую сторону до целого
        private static Int32 CalculateNumOfChunksInFile(string fileName, Int64 chunkSize)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            Decimal dLenght = fileInfo.Length;
            Decimal dChunkSize = chunkSize;
            return (Int32) Math.Ceiling(dLenght/dChunkSize);
        }

        // РАЗБИТЬ ФАЙЛ НА ЧАНКИ
        //  * заполнив позиционную информацию и id (он же порядковый номер)
        public void SplitFileToChunks(ref FileAttributes file, Int64 chunkSize, bool toGenHash, int readWriteBuffer)
        {
            FileInfo fileInfo = new FileInfo(file.Filename);

            if (!File.Exists(file.Filename)) return;

            Int32 chunksInFile = CalculateNumOfChunksInFile(file.Filename, chunkSize);
            file.Chunks = new List<Chunk>(chunksInFile);
            file.FileSize = fileInfo.Length;
            //file.Hash = GenerateFileHash(file.Filename);

            if(chunksInFile < 1)
                throw new Exception("В процессе подсчета количества чанков что-то пошло не так");

            for (int i = 0; i < chunksInFile; i++)
            {
                Chunk tempChunk = new Chunk
                    {
                        Id = i,
                        Start = i*chunkSize,
                        End = chunkSize*(i + 1) > fileInfo.Length ? fileInfo.Length : chunkSize*(i + 1),
                        Length =
                            chunkSize*(i + 1) > fileInfo.Length ? Math.Abs(fileInfo.Length - chunkSize*i) : chunkSize
                    };
                if (toGenHash)
                {
                    tempChunk.Hash = GenerateChunkHash(file.Filename, tempChunk, true, readWriteBuffer);
                    tempChunk.Type = ChunkType.Static;
                }
                file.Chunks.Add(tempChunk);
            }
        }

        // ЗАПОЛНИТЬ СТРУКТУРУ ЧАНКОВ, ЗАПОЛНЕННУЮ ПОЗИЦИОННОЙ ИНФОРМАЦИЕЙ, ХЭШАМИ НА ОСНОВАНИИ FILENAME
        public void GenerateFilesChunksHash(ref FileAttributes filemap, FileAttributes file, int readWriteBuffer)
        {
            for (int i = 0; i < filemap.Chunks.Count(); i++)
            {
                if(filemap.Chunks[i].Type == null)
                    throw new Exception(string.Format("Чанк {0} имеет неопределенный тип", i));

                if (filemap.Chunks[i].Type == ChunkType.Static ||
                    filemap.Chunks[i].Type == ChunkType.Extra)
                {
                    Chunk tempChunk = filemap.Chunks[i];
                    tempChunk.Hash = GenerateChunkHash(file.Filename, filemap.Chunks[i], true, readWriteBuffer);
                    filemap.Chunks[i] = tempChunk;
                }
            }
        }

        // СГЕНЕРИРОВАТЬ ХЭШ МАССИВА БАЙТ
        //  * lenght указывает кол-во байт из массива data
        //  для которых будет генерироваться хэш
        //  * hashAlg - любой из поддерживаемых
        private string GenerateByteArrayHash(HashAlgorithm hashAlg, byte[] data, int lenght)
        {
            byte[] hashData = hashAlg.ComputeHash(data, 0, lenght);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < hashData.Count(); i++)
                stringBuilder.Append(hashData[i].ToString("x2"));
            return stringBuilder.ToString();
        }

        // СГЕНЕРИРОВАТЬ ХЭШ ЧАНКА
        //  * позиционная информация - в chunk
        //  * хэш генерируется на основании файла filename
        private string GenerateChunkHash(string filename, Chunk chunk, bool toSeekFilestream, int readWriteBuffer)
        {
            //var file = new FileStream(filename, FileMode.Open, FileAccess.Read);
            //var hashAlg = MD5.Create();
            //var cs = new CryptoStream(file, hashAlg, CryptoStreamMode.Write);

            //if (toSeekFilestream)
            //    cs.Seek(chunk.Start, SeekOrigin.Begin);

            //long totalBytesRead = 0;
            //byte[] buffer = new byte[readWriteBuffer];

            //while (totalBytesRead <= chunk.Length)
            //{
            //    int bytesToRead = totalBytesRead + buffer.Length > chunk.Length
            //        ? (int) (chunk.Length - totalBytesRead)
            //        : buffer.Length;
            //    totalBytesRead += bytesToRead;
            //    cs.Write(buffer, 0, bytesToRead);
            //}
            //return BitConverter.ToString(hashAlg.Hash);
            MD5 hashAlg = MD5.Create();
            byte[] buffer = new byte[chunk.Length];
            FileStream fileStream = File.OpenRead(filename);

            if (toSeekFilestream)
                fileStream.Seek(chunk.Start, SeekOrigin.Begin);

            fileStream.Read(buffer, 0, (int)chunk.Length);
            fileStream.Close();

            return GenerateByteArrayHash(hashAlg, buffer, (int)chunk.Length);
        }

        // ОПЕРАЦИЯ XOR ДЛЯ ДВУХ ЧАНКОВ
        //  * возвращает тип области, определенный на основании
        //  сравнения
        public string XorChunks(Chunk localChunk, Chunk receivedChunk, string localFile, string receivedFile,
                                Int64 xorUnitSize, bool toSeekReceivedFilestream)
        {
            string chunkType = null;

            FileInfo fiLocalFile = new FileInfo(localFile);
            FileInfo fiReceivedFile = new FileInfo(receivedFile);

            if (fiLocalFile.Length < localChunk.Start + localChunk.Length)
                throw new Exception("Неверные параметры локального чанка");

            if (fiReceivedFile.Length < receivedChunk.Length)
                throw new Exception("Неверные параметры принятого чанка");

            if (localChunk.Length != receivedChunk.Length)
                throw new Exception("Сравниваемые чанки разной длины!");

            byte[] localFileBytes = new byte[xorUnitSize];
            byte[] receivedFileBytes = new byte[xorUnitSize];

            Int64 totalBytesReadLf = 0;
            Int64 totalBytesReadRf = 0;

            using (FileStream fsLocalFile = File.OpenRead(localFile))
            {
                using (FileStream fsReceivedFile = File.OpenRead(receivedFile))
                {
                    fsLocalFile.Seek(localChunk.Start, SeekOrigin.Begin);

                    if (toSeekReceivedFilestream)
                        fsReceivedFile.Seek(receivedChunk.Start, SeekOrigin.Begin);

                    while (totalBytesReadLf < localChunk.Length)
                    {
                        Int32 bytesToRead = totalBytesReadLf + localFileBytes.Length > localChunk.Length
                                                ? (Int32) (localChunk.Length - totalBytesReadLf)
                                                : localFileBytes.Length;

                        Int32 bytesReadLf = fsLocalFile.Read(localFileBytes, 0, bytesToRead);
                        totalBytesReadLf += bytesReadLf;

                        Int32 bytesReadRf = fsReceivedFile.Read(receivedFileBytes, 0, bytesToRead);
                        totalBytesReadRf += bytesReadRf;

                        if (totalBytesReadLf != totalBytesReadRf || bytesReadLf != bytesReadRf)
                            throw new Exception("Непредвиденный размер чанков");

                        if (IsXorUnitsEqual(localFileBytes, receivedFileBytes, bytesReadLf).IsEqual != true)
                        {
                            chunkType = ChunkType.Variable;
                            break;
                        }
                        chunkType = ChunkType.Static;
                    }
                }
            }
            return chunkType;
        }

        // ПРОВЕРКА СУЩЕСТВОВАНИЯ ВСЕХ ФАЙЛОВ С ИМЕНАМИ CHUNKS[I].ID
        // В ПАПКЕ CHUNKSTEMPDIRECTORY
        public bool IsAllFilesExists(string chunksTempDirectory, List<Chunk> chunks)
        {
            bool result = false;

            string[] filesInDir = Directory.GetFiles(chunksTempDirectory);

            foreach (Chunk tempChunk in chunks)
            {
                bool match = false;
                for (int j = 0; j < filesInDir.Count(); j++)
                {
                    Int32 int32ChunkId;
                    Int32.TryParse(Path.GetFileNameWithoutExtension(filesInDir[j]), out int32ChunkId);
                    if (tempChunk.Id != int32ChunkId) continue;
                    match = true;
                    break;
                }
                if (match == false)
                {
                    result = false;
                    break;
                }
                result = true;
            }
            return result;
        }

        // ОПЕРАЦИЯ XOR ДЛЯ ФАЙЛА И ЧАНКОВ ИЗ CHUNKSTEMPDIRECTORY
        //  * результаты заносятся в xorFilemap
        //  * старый xorFilemap обнуляется
        public void XorFileToChunks(FileAttributes localFile, string chunksTempDirectory, ref FileAttributes xorFilemap,
                                    FileAttributes copyFilemap, Int64 xorUnitSize, int readWriteBuffer)
        {
            xorFilemap.Destroy();
            xorFilemap.IsSplitted = true;

            if(localFile.Chunks.Count == 0)
                throw new Exception("Локальный файл не разбит на чанки");

            // проверка наличия всех необходимых чанков
            if(!IsAllFilesExists(chunksTempDirectory, copyFilemap.Chunks))
                throw new Exception("Не все чанки были обнаружены!");

            // операция локальный файл ^ чанки, обнаруженные в copyFilemap
            for (int i = 0; i < localFile.Chunks.Count; i++)
            {
                Chunk tempChunk = localFile.Chunks[i];
                int sameChunkIndex = ChunkPosInChunksArray(localFile.Chunks[i], copyFilemap.Chunks);
                // если чанк локального файла существует в карте копирования - 
                // ксорим и меняем тип
                if (sameChunkIndex >= 0)
                {
                    if (localFile.Chunks[i].Length == copyFilemap.Chunks[sameChunkIndex].Length)
                    {
                        tempChunk = copyFilemap.Chunks[sameChunkIndex];

                        if (tempChunk.Type != ChunkType.Extra)
                        {
                            // добавить распараллеливание
                            tempChunk.Type = XorChunks(localFile.Chunks[i], tempChunk, localFile.Filename,
                                                       Path.Combine(chunksTempDirectory,
                                                                    tempChunk.Id.ToString(CultureInfo.InvariantCulture)),
                                                       xorUnitSize, false);
                        }
                    }
                    else
                    {
                        tempChunk = copyFilemap.Chunks[sameChunkIndex];
                        tempChunk.Type = ChunkType.Variable;
                    }
                }
                // если чанк есть в локальном файле, но его нет в карте копирования -
                // считаем, что чанк статичен
                else
                {
                    tempChunk.Type = ChunkType.Static;
                }
                if (tempChunk.Type == ChunkType.Extra || tempChunk.Type == ChunkType.Static)
                {
                    tempChunk.Hash = GenerateChunkHash(localFile.Filename, tempChunk, true, readWriteBuffer);
                }
                else if (tempChunk.Type == ChunkType.Variable)
                {
                    tempChunk.Hash = null;
                }
                xorFilemap.Chunks.Add(tempChunk);
            }
            // добавить все чанки, отсутствующие в локальном файле но принятые с сервера
            for (int i = 0; i < copyFilemap.Chunks.Count; i++)
            {
                Chunk tempChunk = copyFilemap.Chunks[i];
                int sameChunkIndex = ChunkPosInChunksArray(copyFilemap.Chunks[i], localFile.Chunks);
                if (sameChunkIndex < 0)
                {
                    tempChunk.Type = ChunkType.Extra;
                    xorFilemap.Chunks.Add(tempChunk);
                }
            }
        }

        // ИНДЕКС ЧАНКА TARGET В СПИСКЕ ЧАНКОВ CHUNKS
        //  * параметры, по которым задается схожесть чанков - 
        //      смещение от начала файла и id (т.е. порядковый номер чанка)
        //  * если совпадение не найдено - результат отрицательный
        public int ChunkPosInChunksArray(Chunk target, List<Chunk> chunks)
        {
            int result = -1;
            for (int i = 0; i < chunks.Count; i++)
            {
                if (chunks[i].Start != target.Start || chunks[i].Id != target.Id || chunks[i].End != target.End)
                    continue;
                result = i;
                break;
            }
            return result;
        }

        // ПОБАЙТОВАЯ ОПЕРАЦИЯ ИСКЛЮЧАЮЩЕГО ИЛИ ДЛЯ МАССИВОВ БАЙТ
        private static XorUnitAttributes IsXorUnitsEqual(byte[] xorUnit1, byte[] xorUnit2, int bytesToXor)
        {
            XorUnitAttributes xorUnitAttributes;
            if(xorUnit1.Length != xorUnit2.Length)
                throw new Exception("КсорЮниты имеют разный размер");

            //xorUnitAttributes.IsEqual = StructuralComparisons.StructuralEqualityComparer.Equals(xorUnit1, xorUnit2);
            for (Int64 i = 0; i < bytesToXor; i++)
            {
                if ((byte)(xorUnit1[i] ^ xorUnit2[i]) != 0)
                {
                    xorUnitAttributes.IsEqual = false;
                    xorUnitAttributes.DifferenceOffset = i;
                    return xorUnitAttributes;
                }
            }

            xorUnitAttributes.DifferenceOffset = 0;
            xorUnitAttributes.IsEqual = true;
            return xorUnitAttributes;
        }

        public void ProcessStaticChunk(FileAttributes localFile, FileAttributes xorFilemap, ref Chunk localChunk,
            Chunk xorChunk, FileAttributes copyFilemap, int readWriteBuffer)
        {
            Console.WriteLine("Поток {0} в работе", Thread.CurrentThread.Name);
            if (GenerateChunkHash(localFile.Filename, localChunk, true, readWriteBuffer) != xorChunk.Hash)
            {
                lock (lock1)
                {
                    localChunk.Type = ChunkType.Variable;
                    copyFilemap.Chunks.Add(localChunk);
                    Console.WriteLine("Изменяемый чанк!");
                }
            }
            else
            {
                Console.WriteLine("Статичный чанк!");
            }
        }

        public void Threader()
        {
            Console.WriteLine("Thread created");
        }

        private Thread GetFreeThread(Thread[] threads, ref int threadIndex)
        {
            Thread res = null;

            for (int i = 0; i < threads.Count(); i++)
            {
                if (threads[i] == null)
                {
                    threadIndex = i;
                    res = threads[i];
                    break;
                }
                else if(!threads[i].IsAlive)
                {
                    threadIndex = i;
                    res = threads[i];
                    break;
                }
            }
            return res;
        }

        private void WaitAllThreads(Thread[] threads)
        {
            for (int i = 0; i < threads.Count(); i++)
            {
                if (threads[i] != null)
                {
                    if (threads[i].IsAlive)
                    {
                        Console.WriteLine("Ждем поток {0}", i);
                        threads[i].Join();
                    }
                }
            }
        }

        // СФОРМИРОВАТЬ КАРТУ КОПИРОВАНИЯ
        //  * для статичных областей сравниваются хэши областей актуальной версии локального
        //  файла и хэши соответствующих областей из карты copyFilemap
        public void GenerateCopyFilemap(FileAttributes localFile, FileAttributes xorFilemap,
                                        FileAttributes copyFilemap, int readWriteBuffer)
        {
            Thread[] threadArray = new Thread[6];
            //for(int i=0; i<threadArray.Count(); i++)
            //    threadArray[i] = new Thread(Threader);


            if (localFile.Chunks.Count == 0)
                throw new Exception("Локальный файл не разбит на чанки");

            for (int i = 0; i < localFile.Chunks.Count; i++)
            {
                //Chunk tempChunk = localFile.Chunks[i];
                Chunk tempLocalChunk = localFile.Chunks[i];
                int sameChunkIndex = ChunkPosInChunksArray(localFile.Chunks[i], xorFilemap.Chunks);
                if (sameChunkIndex >= 0)
                {
                    if (xorFilemap.Chunks[sameChunkIndex].Type == ChunkType.Static)
                    {
                        int threadIndex = -1;
                        while (threadIndex < 0)
                        {
                            GetFreeThread(threadArray, ref threadIndex);
                        }

                        threadArray[threadIndex] =
                            new Thread(
                                (() =>
                                    ProcessStaticChunk(localFile, xorFilemap, ref tempLocalChunk,
                                        xorFilemap.Chunks[sameChunkIndex], copyFilemap, readWriteBuffer)));
                        threadArray[threadIndex].Name = "thread_" + threadIndex;
                        threadArray[threadIndex].Start();
                    }
                    else if (xorFilemap.Chunks[sameChunkIndex].Type == ChunkType.Variable)
                    {
                        lock (lock2)
                        {
                            copyFilemap.Chunks.Add(xorFilemap.Chunks[sameChunkIndex]);
                        }
                    }
                }
                else
                {
                    tempLocalChunk.Type = ChunkType.Extra;
                    lock (lock2)
                    {
                        copyFilemap.Chunks.Add(tempLocalChunk);
                    }
                }
            }
            WaitAllThreads(threadArray);
            copyFilemap.FileSize = localFile.FileSize;
            copyFilemap.Filename = localFile.Filename;
        }

        // ИЗВЛЕЧЬ ЧАНКИ ПРЕДНАЗНАЧЕННЫЕ ДЛЯ КОПИРОВАНИЯ
        //  * используются данные из copyFilemap
        public void ExtractChunks(FileAttributes localFile, FileAttributes copyFilemap, string chunkTempDirectory,
                                  int readWriteBuffer)
        {
            for (int i = 0; i < copyFilemap.Chunks.Count; i++)
            {
                if (
                    File.Exists(Path.Combine(chunkTempDirectory,
                                             copyFilemap.Chunks[i].Id.ToString(CultureInfo.InvariantCulture))))
                    throw new Exception("Обнаружены уже извлеченные файлы!");

                CopyUsingBuffer(localFile.Filename, copyFilemap.Chunks[i].Start,
                                Path.Combine(chunkTempDirectory,
                                             copyFilemap.Chunks[i].Id.ToString(CultureInfo.InvariantCulture)), 0,
                                copyFilemap.Chunks[i].Length, readWriteBuffer);
            }
        }

        public Int64 PasteUsingBuffer(string srcFile, Int64 srcOffset, string dstFile, Int64 dstOffset, Int64 bytesToCopy, int readWriteBuffer)
        {
            FileInfo srcFileInfo = new FileInfo(srcFile);

            if (srcFileInfo.Length < bytesToCopy)
                throw new Exception("Кол-во запрошенных байт больше длины файла");

            Int64 totalBytesRead = 0;
            byte[] buffer = new byte[readWriteBuffer];

            using (FileStream srcFs = srcFileInfo.OpenRead())
            {
                using (FileStream dstFs = File.OpenWrite(dstFile))
                {
                    srcFs.Seek(srcOffset, SeekOrigin.Begin);
                    dstFs.Seek(dstOffset, SeekOrigin.Begin);
                    while (totalBytesRead < bytesToCopy && srcFs.Position != srcFs.Length)
                    {
                        Int32 bytesToRead = totalBytesRead + buffer.Length > bytesToCopy
                                                ? (Int32)(bytesToCopy - totalBytesRead)
                                                : buffer.Length;

                        int bytesRead = srcFs.Read(buffer, 0, bytesToRead);
                        dstFs.Write(buffer, 0, bytesRead);
                        totalBytesRead += bytesRead;
                    }
                    dstFs.Close();
                }
                srcFs.Close();
            }
            return totalBytesRead;
        }

        public Int64 CopyUsingBuffer(string srcFile, Int64 srcOffset, string dstFile, Int64 dstOffset, Int64 bytesToCopy, int readWriteBuffer)
        {
            FileInfo srcFileInfo = new FileInfo(srcFile);

            //if (File.Exists(dstFile))
            //    throw new Exception("Файл назначения уже создан!");

            if (srcFileInfo.Length < bytesToCopy)
                throw new Exception("Кол-во запрошенных байт больше длины файла");

            Int64 totalBytesRead = 0;
            byte[] buffer = new byte[readWriteBuffer];

            using (FileStream srcFs = srcFileInfo.OpenRead())
            {
                using (FileStream dstFs = File.Create(dstFile))
                {
                    srcFs.Seek(srcOffset, SeekOrigin.Begin);
                    dstFs.Seek(dstOffset, SeekOrigin.Begin);
                    while (totalBytesRead < bytesToCopy && srcFs.Position != srcFs.Length)
                    {
                        Int32 bytesToRead = totalBytesRead + buffer.Length > bytesToCopy
                                                ? (Int32)(bytesToCopy - totalBytesRead)
                                                : buffer.Length;

                        int bytesRead = srcFs.Read(buffer, 0, bytesToRead);
                        dstFs.Write(buffer, 0, bytesRead);
                        totalBytesRead += bytesRead;
                    }
                }
                srcFs.Close();
            }
            return totalBytesRead;
        }

        // СОЗДАТЬ КАРТУ XOR ПО-УМОЛЧАНИЮ
        //  * разделить локальный файл на чанки
        //  * для каждого чанка сгенерировать md5-хэш
        public void CreateDefaultXorFilemap(FileAttributes localFile, ref FileAttributes xorFilemap, Int64 chunkSize, int readWriteBuffer)
        {
            xorFilemap.CopyFrom(localFile);
            SplitFileToChunks(ref xorFilemap, chunkSize, true, readWriteBuffer);
        }

        // СЛИТЬ ПОЛУЧЕННЫЕ ЧАНКИ С ЛОКАЛЬНЫМ ФАЙЛОМ
        // (ЛИБО ОБРАЗОВАТЬ ЛОКАЛЬНЫЙ ФАЙЛ, СЛИВ ПОЛУЧЕННЫЕ
        // ЧАНКИ)
        //  * если размер итогового файла (из карты copyFilemap)
        //  меньше размера имеющегося файла - копируется область размером
        //  copyFilemap.FileSize из имеющегося файла
        //  * к обрабатываемому локальному файлу добавляются 
        //  принятые чанки
        public void MergeChunks(FileAttributes localFile, FileAttributes copyFilemap, ref FileAttributes mergedFile,
                                string chunkTempDirectory,
                                int readWriteBuffer)
        {
            if (copyFilemap.Chunks.Count == 0)
                throw new Exception("Пустая карта копирования!");

            // проверка наличия всех необходимых чанков
            if (!IsAllFilesExists(chunkTempDirectory, copyFilemap.Chunks))
                throw new Exception("Не все чанки были обнаружены!");

            // если локальный файл не существует -
            // просто соединяем все чанки из папки чанков в соответствии
            // с информацией из copyFilemap
            if (!File.Exists(localFile.Filename))
            {
                Int64 totalBytesRead = 0;
                for (int i = 0; i < copyFilemap.Chunks.Count; i++)
                {
                    Int64 bytesCopied =
                        PasteUsingBuffer(
                            Path.Combine(chunkTempDirectory,
                                copyFilemap.Chunks[i].Id.ToString(CultureInfo.InvariantCulture)), 0,
                            localFile.Filename, totalBytesRead, copyFilemap.Chunks[i].Length, readWriteBuffer);
                    totalBytesRead += bytesCopied;
                    Console.WriteLine("Слито " + totalBytesRead + " байт");
                }
                return;
            }
            // если размер результирующего файла
            // меньше размера неповторимого оригинала - 
            // копируем необходимого размера кусок из старого файла в новый
            if (copyFilemap.FileSize < localFile.FileSize)
            {
                CopyUsingBuffer(localFile.Filename, 0, mergedFile.Filename, 0, copyFilemap.FileSize, readWriteBuffer);
            }
                    
            // добавить новые чанки
            for (int i = 0; i < copyFilemap.Chunks.Count; i++)
            {
                PasteUsingBuffer(Path.Combine(chunkTempDirectory, copyFilemap.Chunks[i].Id.ToString(CultureInfo.InvariantCulture)), 0, localFile.Filename,
                                 copyFilemap.Chunks[i].Start, copyFilemap.Chunks[i].Length, readWriteBuffer);
            }
        }

        // ПРОВЕРИТЬ СУЩЕСТВОВАНИЕ ФАЙЛА
        public bool IsFileExists(FileAttributes file)
        {
            if (File.Exists(file.Filename))
                return true;
            return false;
        }
    }
}

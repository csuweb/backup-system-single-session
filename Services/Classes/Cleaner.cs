﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Services.Classes
{
    public static class Cleaner
    {
        public static void Clear(string chunksTempFolder, FileAttributes copyFilemap)
        {
            for (int i = 0; i < copyFilemap.Chunks.Count(); i++)
            {
                string tempPath;
                if (File.Exists(tempPath = Path.Combine(chunksTempFolder, copyFilemap.Chunks[i].Id.ToString())))
                {
                    File.Delete(tempPath);
                    Console.WriteLine("Файл {0} вычищен", tempPath);
                }
                else
                {
                    Console.WriteLine("Не удалось вычистить файл {0}", tempPath);
                }
            }
            if (File.Exists(copyFilemap.FileMapName))
            {
                File.Delete(copyFilemap.FileMapName);
                Console.WriteLine("Карта копирования удалена");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Services.Classes
{
    public class Viewer
    {
        private Logger _logger;

        public Viewer(Logger lg)
        {
            _logger = lg;
        }

        private const string InfoPrefix = "[ИНФО] ";

        public void DescribeFile(FileAttributes file)
        {
            string message = string.Format("----------FILEINFO----------{0}" +
                                           "[Имя файла]: {1}{2}" +
                                           "[Имя ассоц. карты]: {3}{4}" +
                                           "[Размер]: {5}{6}" +
                                           "[MD5-Хэш]: {7}{8}" +
                                           "[Кол-во чанков]: {9}{10}" +
                                           "---------------------------",
                                           Environment.NewLine,
                                           file.Filename,
                                           Environment.NewLine,
                                           file.FileMapName,
                                           Environment.NewLine,
                                           file.FileSize,
                                           Environment.NewLine,
                                           file.Hash,
                                           Environment.NewLine,
                                           file.Chunks.Count,
                                           Environment.NewLine);
            Console.WriteLine(message);
            _logger.LogString(message, true);
        }

        public void DescribeInitSettings(string targetFile, long chunkSize, int readWriteBuffer)
        {
            string message = string.Format("Целевой файл - \"{0}\"{1}" +
                                           "Размер чанка - {2}{3}" +
                                           "Размер буфера - {4}",
                                           targetFile,
                                           Environment.NewLine,
                                           chunkSize,
                                           Environment.NewLine,
                                           readWriteBuffer);
            Console.WriteLine(message);
            _logger.LogString(message, true);
        }

        public void PrintInfo(string message, bool newLine)
        {
            string lMessage = InfoPrefix + message;
            if(newLine)
                Console.WriteLine(lMessage);
            else
                Console.Write(lMessage);
            _logger.LogString(lMessage, newLine);
        }

    }
}
